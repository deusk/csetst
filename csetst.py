# Copyright (C) 2018  Qian Cai <qian deusk org>
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function
import os
import subprocess
import re
import textwrap

Final = 0

def cse_search(lines = ""):
    global Final
    for line in lines.splitlines():
        # Avoid "[" becoming a part of regular expressions.
        line = line.replace("[", "\[")
        # sesearch 4.0+ changes output format."
        line = line.replace(" : ", " *: *")
        line = line.replace("; ", "; *")
        line = line.replace(" ;", " *;")
        match = re.search(line, POLICY)
        if not match:
            print("- error: unexpected sesearch " + line)
            Final += 1


# Check modules.
subprocess.check_call(["semodule", "-X200", "-r", "container"])
subprocess.check_call(["semodule", "-X200", "-i",
    "/usr/share/selinux/packages/container.pp.bz2"])

if not os.path.isfile("/usr/bin/sesearch"):
    subprocess.check_call(["yum", "-y", "install", "setools-console"])
fp = open(os.devnull, "w")
try:
    POLICY = subprocess.check_output(["sesearch", "-A", "-C"],
        stderr=fp).decode("ascii")
except:
    # sesearch 4.0+ changes options.
    POLICY = subprocess.check_output(["sesearch", "--allow"]).decode("ascii")
fp.close()

out = subprocess.check_output(["getsebool",
    "container_manage_cgroup",
    "container_connect_any",
    "virt_use_nfs",
    "virt_use_samba",
    "virt_sandbox_use_fusefs",
    "virt_sandbox_use_sys_admin",
    "virt_sandbox_use_netlink",
    "virt_sandbox_use_audit",
    "virt_sandbox_use_all_caps",
    "virt_sandbox_use_mknod"]).decode("ascii")
if out != textwrap.dedent("""\
    container_manage_cgroup --> off
    container_connect_any --> off
    virt_use_nfs --> on
    virt_use_samba --> off
    virt_sandbox_use_fusefs --> off
    virt_sandbox_use_sys_admin --> off
    virt_sandbox_use_netlink --> off
    virt_sandbox_use_audit --> on
    virt_sandbox_use_all_caps --> on
    virt_sandbox_use_mknod --> off\n"""):
        print("- error: unexpected getsebool output is " + out)
        Final += 1

cse_search(lines = """\
allow container_t cgroup_t : dir { .* } ; [ container_manage_cgroup ]
allow container_t cgroup_t : lnk_file { .* } ; [ container_manage_cgroup ]
allow container_t cgroup_t : file { .* } ; [ container_manage_cgroup ]
allow container_t sysfs_t : dir { .* } ; [ container_manage_cgroup ]
allow container_t tmpfs_t : dir { .* } ; [ container_manage_cgroup ]
allow container_runtime_t packet_type : packet recv ; [ container_connect_any ]
allow container_runtime_t packet_type : packet send ; [ container_connect_any ]
allow container_runtime_t port_type : tcp_socket name_connect ; [ container_connect_any ]
allow container_runtime_t port_type : tcp_socket { .* } ; [ container_connect_any ]
allow container_runtime_t autofs_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : file { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : file { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : filesystem mount ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : filesystem unmount ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : lnk_file { .* } ; [ virt_use_nfs ]
allow container_runtime_t nfs_t : sock_file { .* } ; [ virt_use_nfs ]
allow container_runtime_t proc_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t sysctl_fs_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t sysctl_fs_t : dir { .* } ; [ virt_use_nfs ]
allow container_runtime_t sysctl_fs_t : file { .* } ; [ virt_use_nfs ]
allow container_runtime_t sysctl_t : dir { .* } ; [ virt_use_nfs ]
allow svirt_sandbox_domain autofs_t : dir { .* } ; [ virt_use_nfs ]
allow svirt_sandbox_domain nfs_t : dir { .* } ; [ virt_use_nfs ]
allow svirt_sandbox_domain nfs_t : dir { .* } ; [ virt_use_nfs ]
allow svirt_sandbox_domain nfs_t : file { .* } ; [ virt_use_nfs ]
allow svirt_sandbox_domain nfs_t : lnk_file { .* } ; [ virt_use_nfs ]
allow svirt_sandbox_domain nfs_t : sock_file { .* } ; [ virt_use_nfs ]
allow container_runtime_t cifs_t : dir { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : dir { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : dir { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : dir { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : dir { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : file { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : file { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : lnk_file { .* } ; [ virt_use_samba ]
allow container_runtime_t cifs_t : sock_file { .* } ; [ virt_use_samba ]
allow svirt_sandbox_domain cifs_t : dir { .* } ; [ virt_use_samba ]
allow svirt_sandbox_domain cifs_t : dir { .* } ; [ virt_use_samba ]
allow svirt_sandbox_domain cifs_t : dir { .* } ; [ virt_use_samba ]
allow svirt_sandbox_domain cifs_t : file { .* } ; [ virt_use_samba ]
allow svirt_sandbox_domain cifs_t : lnk_file { .* } ; [ virt_use_samba ]
allow svirt_sandbox_domain cifs_t : sock_file { .* } ; [ virt_use_samba ]
allow container_runtime_t fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow container_runtime_t fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow container_runtime_t fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow container_runtime_t fusefs_t : file { .* } ; [ virt_sandbox_use_fusefs ]
allow container_runtime_t fusefs_t : file { .* } ; [ virt_sandbox_use_fusefs ]
allow container_runtime_t fusefs_t : filesystem mount ; [ virt_sandbox_use_fusefs ]
allow container_runtime_t fusefs_t : filesystem unmount ; [ virt_sandbox_use_fusefs ]
allow container_runtime_t fusefs_t : lnk_file { .* } ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : file { .* } ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : file { .* } ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : filesystem mount ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : filesystem unmount ; [ virt_sandbox_use_fusefs ]
allow container_t fusefs_t : lnk_file { .* } ; [ virt_sandbox_use_fusefs ]
allow svirt_sandbox_domain fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow svirt_sandbox_domain fusefs_t : dir { .* } ; [ virt_sandbox_use_fusefs ]
allow svirt_sandbox_domain fusefs_t : file { .* } ; [ virt_sandbox_use_fusefs ]
allow svirt_sandbox_domain fusefs_t : lnk_file { .* } ; [ virt_sandbox_use_fusefs ]
allow container_t container_t : capability sys_admin ; [ virt_sandbox_use_sys_admin ]
allow container_t container_t : cap_userns sys_admin ; [ virt_sandbox_use_sys_admin ]
allow container_t container_t : netlink_kobject_uevent_socket { .* } ; [ virt_sandbox_use_netlink ]
allow container_t container_t : netlink_socket { .* } ; [ virt_sandbox_use_netlink ]
allow container_t container_t : netlink_tcpdiag_socket { .* } ; [ virt_sandbox_use_netlink ]
allow container_t container_t : capability audit_write ; [ virt_sandbox_use_audit ]
allow container_t container_t : netlink_audit_socket { .* } ; [ virt_sandbox_use_audit ]
allow container_t container_t : cap2_userns { .* } ; [ virt_sandbox_use_all_caps ]
allow container_t container_t : capability2 { .* } ; [ virt_sandbox_use_all_caps ]
allow container_t container_t : capability { .* } ; [ virt_sandbox_use_all_caps ]
allow container_t container_t : cap_userns { .* } ; [ virt_sandbox_use_all_caps ]
allow container_t container_t : capability mknod ; [ virt_sandbox_use_mknod ]
allow container_t container_t : cap_userns mknod ; [ virt_sandbox_use_mknod ]
allow container_domain container_share_t : dir { .* } ;
allow container_domain container_share_t : file { .* } ;
allow container_domain container_share_t : lnk_file { .* } ;
allow container_domain file_type : filesystem getattr ;
allow svirt_sandbox_domain container_share_t : dir { .* } ;
allow svirt_sandbox_domain container_share_t : file { .* } ;
allow svirt_sandbox_domain container_share_t : lnk_file { .* } ;
allow svirt_sandbox_domain file_type : dir { .* } ;
allow svirt_sandbox_domain file_type : filesystem getattr ;
allow svirt_sandbox_domain mountpoint : dir { .* } ;
allow container_domain anon_inodefs_t : dir { .* } ; 
allow container_domain anon_inodefs_t : file { .* } ; 
allow container_domain console_device_t : chr_file { .* } ; 
allow container_domain container_devpts_t : chr_file { .* } ; 
allow container_domain container_file_t : blk_file { .* } ; 
allow container_domain container_file_t : chr_file { .* } ; 
allow container_domain container_file_t : dir { .* } ; 
allow container_domain container_file_t : fifo_file { .* } ; 
allow container_domain container_file_t : file { .* } ; 
allow container_domain container_file_t : filesystem { .* } ; 
allow container_domain container_file_t : lnk_file { .* } ; 
allow container_domain container_file_t : sock_file { .* } ; 
allow container_domain container_runtime_t : fd use ; 
allow container_domain container_runtime_t : fifo_file { .* } ; 
allow container_domain container_runtime_tmpfs_t : dir { .* } ; 
allow container_domain container_runtime_tmpfs_t : lnk_file { .* } ; 
allow container_domain container_runtime_tmpfs_t : sock_file { .* } ; 
allow container_domain container_runtime_t : unix_stream_socket connectto ; 
allow container_domain container_share_t : dir { .* } ; 
allow container_domain container_share_t : file { .* } ; 
allow container_domain container_share_t : lnk_file { .* } ; 
allow container_domain container_var_run_t : dir { .* } ; 
allow container_domain container_var_run_t : sock_file { .* } ; 
allow container_domain device_t : dir { .* } ; 
allow container_domain devpts_t : chr_file { .* } ; 
allow container_domain filesystem_type : filesystem getattr ; 
allow container_domain file_type : filesystem getattr ; 
allow container_domain hugetlbfs_t : dir { .* } ; 
allow container_domain hugetlbfs_t : file { .* } ; 
allow container_domain inotifyfs_t : dir { .* } ; 
allow container_domain kernel_t : system ipc_info ; 
allow container_domain mtrr_device_t : chr_file { .* } ; 
allow container_domain mtrr_device_t : file { .* } ; 
allow container_domain pidfile : sock_file { .* } ; 
allow container_domain proc_net_t : dir { .* } ; 
allow container_domain proc_net_t : file { .* } ; 
allow container_domain proc_net_t : lnk_file { .* } ; 
allow container_domain proc_t : dir { .* } ; 
allow container_domain proc_t : filesystem getattr ; 
allow container_domain proc_type : dir { .* } ; 
allow container_domain proc_type : file getattr ; 
allow container_domain ptynode : chr_file { .* } ; 
allow container_domain spc_t : unix_stream_socket connectto ; 
allow container_domain sysctl_irq_t : dir { .* } ; 
allow container_domain sysctl_irq_t : file { .* } ; 
allow container_domain sysctl_net_t : dir { .* } ; 
allow container_domain sysctl_net_t : file { .* } ; 
allow container_domain sysctl_net_t : lnk_file { .* } ; 
allow container_domain sysctl_net_unix_t : file { .* } ; 
allow container_domain sysctl_rpc_t : dir { .* } ; 
allow container_domain sysctl_rpc_t : file { .* } ; 
allow container_domain sysctl_t : dir { .* } ; 
allow container_domain sysctl_type : dir { .* } ; 
allow container_domain sysctl_type : file { .* } ; 
allow container_domain sysfs_t : dir { .* } ; 
allow container_domain sysfs_t : file { .* } ; 
allow container_domain sysfs_t : lnk_file { .* } ; 
allow container_domain tmpfs_t : dir { .* } ; 
allow container_domain tmpfs_t : file { .* } ; 
allow container_domain tmpfs_t : lnk_file { .* } ; 
allow container_domain tty_device_t : chr_file { .* } ; 
allow container_domain ttynode : chr_file { .* } ; 
allow container_domain user_devpts_t : chr_file { .* } ; 
allow container_domain var_lib_t : dir { .* } ; 
allow container_domain var_run_t : dir { .* } ; 
allow container_domain var_run_t : lnk_file { .* } ; 
allow container_domain var_t : dir { .* } ; 
allow container_domain var_t : lnk_file { .* } ; 
allow container_net_domain node_t : rawip_socket node_bind ; 
allow container_net_domain node_t : tcp_socket node_bind ; 
allow container_net_domain node_t : udp_socket node_bind ; 
allow container_net_domain port_type : tcp_socket { .* } ; 
allow container_net_domain port_type : udp_socket { .* } ; 
allow container_t cgroup_t : dir { .* } ; 
allow container_t cgroup_t : file { .* } ; 
allow container_t cgroup_t : lnk_file { .* } ; 
allow container_t container_t : appletalk_socket { .* } ; 
allow container_t container_t : association sendto ; 
allow container_t container_t : capability { .* } ; 
allow container_t container_t : cap_userns { .* } ; 
allow container_t container_t : dccp_socket { .* } ; 
allow container_t container_t : dir { .* } ; 
allow container_t container_t : fifo_file { .* } ; 
allow container_t container_t : file { .* } ; 
allow container_t container_t : filesystem associate ; 
allow container_t container_t : key { .* } ; 
allow container_t container_t : lnk_file { .* } ; 
allow container_t container_t : msgq { .* } ; 
allow container_t container_t : msg { .* } ; 
allow container_t container_t : netlink_audit_socket { .* } ; 
allow container_t container_t : netlink_connector_socket { .* } ; 
allow container_t container_t : netlink_crypto_socket { .* } ; 
allow container_t container_t : netlink_dnrt_socket { .* } ; 
allow container_t container_t : netlink_fib_lookup_socket { .* } ; 
allow container_t container_t : netlink_firewall_socket { .* } ; 
allow container_t container_t : netlink_generic_socket { .* } ; 
allow container_t container_t : netlink_ip6fw_socket { .* } ; 
allow container_t container_t : netlink_iscsi_socket { .* } ; 
allow container_t container_t : netlink_kobject_uevent_socket { .* } ; 
allow container_t container_t : netlink_netfilter_socket { .* } ; 
allow container_t container_t : netlink_nflog_socket { .* } ; 
allow container_t container_t : netlink_rdma_socket { .* } ; 
allow container_t container_t : netlink_route_socket { .* } ; 
allow container_t container_t : netlink_scsitransport_socket { .* } ; 
allow container_t container_t : netlink_selinux_socket { .* } ; 
allow container_t container_t : netlink_socket { .* } ; 
allow container_t container_t : netlink_tcpdiag_socket { .* } ; 
allow container_t container_t : netlink_xfrm_socket { .* } ; 
allow container_t container_t : packet_socket { .* } ; 
allow container_t container_t : passwd rootok ; 
allow container_t container_t : peer recv ; 
allow container_t container_t : process { .* } ; 
allow container_t container_t : rawip_socket { .* } ; 
allow container_t container_t : sem { .* } ; 
allow container_t container_t : shm { .* } ; 
allow container_t container_t : socket { .* } ; 
allow container_t container_t : tcp_socket { .* } ; 
allow container_t container_t : tun_socket { .* } ; 
allow container_t container_t : udp_socket { .* } ; 
allow container_t container_t : unix_dgram_socket { .* } ; 
allow container_t container_t : unix_stream_socket { .* } ; 
allow container_t device_t : dir { .* } ; 
allow container_t etc_t : dir { .* } ; 
allow container_t init_var_run_t : dir { .* } ; 
allow container_t modules_object_t : dir { .* } ; 
allow container_t modules_object_t : file { .* } ; 
allow container_t modules_object_t : lnk_file { .* } ; 
allow container_t net_conf_t : dir { .* } ; 
allow container_t net_conf_t : file { .* } ; 
allow container_t net_conf_t : lnk_file { .* } ; 
allow container_t pidfile : dir { .* } ; 
allow container_t proc_kmsg_t : file { .* } ; 
allow container_t proc_t : dir { .* } ; 
allow container_t random_device_t : chr_file { .* } ; 
allow container_t sssd_t : unix_stream_socket connectto ; 
allow container_t sssd_var_lib_t : dir { .* } ; 
allow container_t sssd_var_lib_t : sock_file { .* } ; 
allow container_t sysfs_t : dir { .* } ; 
allow container_t systemd_logind_t : dbus send_msg ; 
allow container_t systemd_logind_t : fd use ; 
allow container_t tmpfs_t : dir { .* } ; 
allow container_t urandom_device_t : chr_file { .* } ; 
allow container_t var_run_t : dir { .* } ; 
allow container_t var_run_t : lnk_file { .* } ; 
allow container_t var_t : dir { .* } ; 
allow container_t var_t : lnk_file { .* } ; 
allow sandbox_net_domain node_t : rawip_socket node_bind ;
allow sandbox_net_domain node_t : tcp_socket node_bind ;
allow sandbox_net_domain node_t : udp_socket node_bind ;
allow sandbox_net_domain port_type : tcp_socket { .* } ;
allow sandbox_net_domain port_type : udp_socket { .* } ;
allow sandbox_net_domain proc_net_t : dir { .* } ;
allow sandbox_net_domain proc_net_t : file { .* } ;
allow sandbox_net_domain proc_net_t : lnk_file { .* } ;
allow sandbox_net_domain proc_t : dir { .* } ;
allow sandbox_net_domain sssd_t : unix_stream_socket connectto ;
allow sandbox_net_domain sssd_var_lib_t : dir { .* } ;
allow sandbox_net_domain sssd_var_lib_t : sock_file { .* } ;
allow sandbox_net_domain svirt_home_t : dir { .* } ;
allow sandbox_net_domain svirt_home_t : fifo_file { .* } ;
allow sandbox_net_domain svirt_home_t : file { .* } ;
allow sandbox_net_domain svirt_home_t : lnk_file { .* } ;
allow sandbox_net_domain svirt_home_t : sock_file { .* } ;
allow sandbox_net_domain systemd_logind_t : dbus send_msg ;
allow sandbox_net_domain systemd_logind_t : fd use ;
allow sandbox_net_domain var_run_t : dir { .* } ;
allow sandbox_net_domain var_run_t : lnk_file { .* } ;
allow sandbox_net_domain var_t : dir { .* } ;
allow sandbox_net_domain var_t : lnk_file { .* } ;
allow sandbox_net_domain virt_home_t : dir { .* } ;
allow svirt_sandbox_domain anon_inodefs_t : dir { .* } ;
allow svirt_sandbox_domain anon_inodefs_t : file { .* } ;
allow svirt_sandbox_domain bin_t : dir { .* } ;
allow svirt_sandbox_domain container_devpts_t : chr_file { .* } ;
allow svirt_sandbox_domain container_file_t : blk_file { .* } ;
allow svirt_sandbox_domain container_file_t : chr_file { .* } ;
allow svirt_sandbox_domain container_file_t : dir { .* } ;
allow svirt_sandbox_domain container_file_t : fifo_file { .* } ;
allow svirt_sandbox_domain container_file_t : file { .* } ;
allow svirt_sandbox_domain container_file_t : lnk_file { .* } ;
allow svirt_sandbox_domain container_file_t : sock_file { .* } ;
allow svirt_sandbox_domain container_runtime_t : dir { .* } ;
allow svirt_sandbox_domain container_runtime_t : fd use ;
allow svirt_sandbox_domain container_runtime_t : fifo_file { .* } ;
allow svirt_sandbox_domain container_runtime_t : file { .* } ;
allow svirt_sandbox_domain container_runtime_t : lnk_file { .* } ;
allow svirt_sandbox_domain container_runtime_t : process { .* } ;
allow svirt_sandbox_domain container_share_t : dir { .* } ;
allow svirt_sandbox_domain container_share_t : file { .* } ;
allow svirt_sandbox_domain container_share_t : lnk_file { .* } ;
allow svirt_sandbox_domain container_var_lib_t : dir { .* } ;
allow svirt_sandbox_domain device_t : dir { .* } ;
allow svirt_sandbox_domain device_t : lnk_file { .* } ;
allow svirt_sandbox_domain exec_type : file { .* } ;
allow svirt_sandbox_domain exec_type : lnk_file { .* } ;
allow svirt_sandbox_domain filesystem_type : filesystem getattr ;
allow svirt_sandbox_domain file_type : dir { .* } ;
allow svirt_sandbox_domain file_type : filesystem getattr ;
allow svirt_sandbox_domain fonts_cache_t : dir { .* } ;
allow svirt_sandbox_domain fonts_cache_t : file { .* } ;
allow svirt_sandbox_domain fonts_cache_t : lnk_file { .* } ;
allow svirt_sandbox_domain fonts_t : dir { .* } ;
allow svirt_sandbox_domain fonts_t : file { .* } ;
allow svirt_sandbox_domain fonts_t : lnk_file { .* } ;
allow svirt_sandbox_domain fusefs_t : dir { .* } ;
allow svirt_sandbox_domain fusefs_t : file { .* } ;
allow svirt_sandbox_domain httpd_modules_t : dir { .* } ;
allow svirt_sandbox_domain httpd_modules_t : file { .* } ;
allow svirt_sandbox_domain httpd_modules_t : lnk_file { .* } ;
allow svirt_sandbox_domain httpd_sys_content_t : dir { .* } ;
allow svirt_sandbox_domain httpd_sys_content_t : file { .* } ;
allow svirt_sandbox_domain httpd_sys_content_t : lnk_file { .* } ;
allow svirt_sandbox_domain hugetlbfs_t : dir { .* } ;
allow svirt_sandbox_domain hugetlbfs_t : file { .* } ;
allow svirt_sandbox_domain hwdata_t : dir { .* } ;
allow svirt_sandbox_domain hwdata_t : file { .* } ;
allow svirt_sandbox_domain hwdata_t : lnk_file { .* } ;
allow svirt_sandbox_domain inotifyfs_t : dir { .* } ;
allow svirt_sandbox_domain lib_t : dir { .* } ;
allow svirt_sandbox_domain lib_t : lnk_file { .* } ;
allow svirt_sandbox_domain mountpoint : dir { .* } ;
allow svirt_sandbox_domain onload_fs_t : dir { .* } ;
allow svirt_sandbox_domain onload_fs_t : sock_file { .* } ;
allow svirt_sandbox_domain pam_var_console_t : dir { .* } ;
allow svirt_sandbox_domain pidfile : sock_file { .* } ;
allow svirt_sandbox_domain proc_net_t : dir { .* } ;
allow svirt_sandbox_domain proc_t : dir { .* } ;
allow svirt_sandbox_domain proc_t : filesystem getattr ;
allow svirt_sandbox_domain proc_type : dir { .* } ;
allow svirt_sandbox_domain spc_t : fd use ;
allow svirt_sandbox_domain spc_t : fifo_file { .* } ;
allow svirt_sandbox_domain spc_t : process sigchld ;
allow svirt_sandbox_domain spc_t : unix_stream_socket connectto ;
allow svirt_sandbox_domain sshd_devpts_t : chr_file { .* } ;
allow svirt_sandbox_domain sshd_t : dir { .* } ;
allow svirt_sandbox_domain sshd_t : fd use ;
allow svirt_sandbox_domain sshd_t : fifo_file { .* } ;
allow svirt_sandbox_domain sshd_t : file { .* } ;
allow svirt_sandbox_domain sshd_t : lnk_file { .* } ;
allow svirt_sandbox_domain sshd_t : process { .* } ;
allow svirt_sandbox_domain sysadm_t : fd use ;
allow svirt_sandbox_domain sysadm_t : fifo_file { .* } ;
allow svirt_sandbox_domain sysadm_t : process sigchld ;
allow svirt_sandbox_domain sysctl_net_t : dir { .* } ;
allow svirt_sandbox_domain sysctl_net_t : file { .* } ;
allow svirt_sandbox_domain sysctl_net_t : lnk_file { .* } ;
allow svirt_sandbox_domain sysctl_net_unix_t : file { .* } ;
allow svirt_sandbox_domain sysctl_t : dir { .* } ;
allow svirt_sandbox_domain sysctl_type : dir { .* } ;
allow svirt_sandbox_domain sysctl_type : file { .* } ;
allow svirt_sandbox_domain systemd_machined_t : dir { .* } ;
allow svirt_sandbox_domain systemd_machined_t : file { .* } ;
allow svirt_sandbox_domain systemd_machined_t : lnk_file { .* } ;
allow svirt_sandbox_domain systemd_machined_t : process getattr ;
allow svirt_sandbox_domain tmpfs_t : dir { .* } ;
allow svirt_sandbox_domain tmpfs_t : file { .* } ;
allow svirt_sandbox_domain tmpfs_t : lnk_file { .* } ;
allow svirt_sandbox_domain udev_var_run_t : dir { .* } ;
allow svirt_sandbox_domain udev_var_run_t : file { .* } ;
allow svirt_sandbox_domain udev_var_run_t : lnk_file { .* } ;
allow svirt_sandbox_domain unconfined_service_t : fd use ;
allow svirt_sandbox_domain unconfined_service_t : fifo_file { .* } ;
allow svirt_sandbox_domain unconfined_service_t : process sigchld ;
allow svirt_sandbox_domain unconfined_t : fd use ;
allow svirt_sandbox_domain unconfined_t : fifo_file { .* } ;
allow svirt_sandbox_domain unconfined_t : process sigchld ;
allow svirt_sandbox_domain user_devpts_t : chr_file { .* } ;
allow svirt_sandbox_domain user_tty_device_t : chr_file { .* } ;
allow svirt_sandbox_domain usr_t : dir { .* } ;
allow svirt_sandbox_domain usr_t : lnk_file { .* } ;
allow svirt_sandbox_domain var_lib_t : dir { .* } ;
allow svirt_sandbox_domain var_lock_t : dir { .* } ;
allow svirt_sandbox_domain var_lock_t : lnk_file { .* } ;
allow svirt_sandbox_domain var_run_t : dir { .* } ;
allow svirt_sandbox_domain var_run_t : lnk_file { .* } ;
allow svirt_sandbox_domain var_t : dir { .* } ;
allow svirt_sandbox_domain var_t : lnk_file { .* } ;
allow svirt_sandbox_domain virsh_t : fd use ;
allow svirt_sandbox_domain virsh_t : fifo_file { .* } ;
allow svirt_sandbox_domain virsh_t : process sigchld ;
allow svirt_sandbox_domain virtd_lxc_t : fd use ;
allow svirt_sandbox_domain virtd_lxc_t : process sigchld ;
allow svirt_sandbox_domain virtd_lxc_t : unix_stream_socket { .* } ;""")

exit(Final)
